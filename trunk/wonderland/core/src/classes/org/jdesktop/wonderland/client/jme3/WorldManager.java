/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jdesktop.wonderland.client.jme3;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 *
 * @author cramirez
 */
public class WorldManager {

    private Node rootNode;
    
    public void removeSpatial(Spatial spatial) {
    }

    public void addSpatial(Spatial spatial) {
    }

    /**
     * @return the rootNode
     */
    public Node getRootNode() {
        return rootNode;
    }

    /**
     * @param rootNode the rootNode to set
     */
    public void setRootNode(Node rootNode) {
        this.rootNode = rootNode;
    }

    
}
