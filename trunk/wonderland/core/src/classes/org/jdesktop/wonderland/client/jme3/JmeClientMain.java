/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jdesktop.wonderland.client.jme3;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.UrlLocator;
import com.jme3.math.Vector3f;
import com.jme3.math.Quaternion;
import org.jdesktop.wonderland.client.jme3.login.JmeLoginUI;

import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import org.jdesktop.mtgame.processor.WorkProcessor;
import org.jdesktop.wonderland.client.ClientContext;
import org.jdesktop.wonderland.client.comms.LoginFailureException;
import org.jdesktop.wonderland.client.comms.WonderlandSession.Status;
import org.jdesktop.wonderland.client.input.InputManager;
import org.jdesktop.wonderland.client.jme.SceneWorker;
import org.jdesktop.wonderland.client.login.LoginManager;
import org.jdesktop.wonderland.client.login.ServerSessionManager;

/**
 *
 * @author cramirez
 */
public class JmeClientMain extends SimpleApplication {
    
    public static final String SERVER_URL_PROP = "sgs.server";
    private static final Logger LOGGER =
            Logger.getLogger(JmeClientMain.class.getName());

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(
            "org/jdesktop/wonderland/client/jme/resources/Bundle");

    /** Default window size */
    private static final int DEFAULT_WIDTH = 800;
    private static final int DEFAULT_HEIGHT = 600;

    /** The frame of the Wonderland client window. */
    private static MainFrame frame;
    
    // standard properties
    private static final String PROPS_URL_PROP = "run.properties.file";
    private static final String CONFIG_DIR_PROP =
            "jnlp.wonderland.client.config.dir";
    private static final String DESIRED_FPS_PROP = "wonderland.client.fps";
    private static final String WINDOW_SIZE_PROP =
            "wonderland.client.windowSize";
    // default values
    private static final String SERVER_URL_DEFAULT = "http://localhost:8080";
    private static final String DESIRED_FPS_DEFAULT = "30";
    private static final String WINDOW_SIZE_DEFAULT = "800x600";
  
    // the current Wonderland login and session
    private JmeLoginUI login;
    private JmeClientSession curSession;
    // keep tack of whether we are currently logging out
    private boolean loggingOut;
    // whether we should try to auto-reconnect
    private boolean autoReconnect = true;
    
    
    public static void main(String[] args) {
        JmeClientMain app = new JmeClientMain();
        
        app.setDisplayFps(false);
        app.setDisplayStatView(false);
        app.setShowSettings(false);
        
        app.start();
    }

    @Override
    public void simpleInitApp() {

        assetManager.registerLocator("",
                                WlaLocator.class);
        
        String windowSize = System.getProperty(
                WINDOW_SIZE_PROP, WINDOW_SIZE_DEFAULT);
        
        int width = DEFAULT_WIDTH;
        int height = DEFAULT_HEIGHT;

        try {
            if (windowSize.equalsIgnoreCase("fullscreen")) {
                // OWL issue #146: for full screen, let the frame figure out
                // how to size itself
                width = -1;
                height = -1; // -50 hack for current swing decorations
            } else {
                String sizeWidth =
                        windowSize.substring(0, windowSize.indexOf('x'));
                String sizeHeight =
                        windowSize.substring(windowSize.indexOf('x') + 1);
                width = Integer.parseInt(sizeWidth);
                height = Integer.parseInt(sizeHeight);
            }
        } catch (Exception e) {
            LOGGER.warning(WINDOW_SIZE_PROP
                    + " error, should be of the form 640x480 (or fullscreen), "
                    + "instead of the current " + windowSize);
        }

        // make sure the server URL is set
        String serverURL = System.getProperty(SERVER_URL_PROP);
        if (serverURL == null) {
            serverURL = SERVER_URL_DEFAULT;
            System.setProperty(SERVER_URL_PROP, serverURL);
        }

        // set up the context
        ClientContextJME.setClientMain(this);

        WorldManager worldManager = ClientContextJME.getWorldManager();
//        worldManager.getRenderManager().setDesiredFrameRate(getDesiredFrameRate());

        createUI(worldManager, width, height);
        
        // Register our loginUI for login requests
        login = new JmeLoginUI(frame);
        LoginManager.setLoginUI(login);
        
        // load the starting coordinates and look direction
        float startX = readJnlpProperty("x");
        float startY = readJnlpProperty("y");
        float startZ = readJnlpProperty("z");
        Vector3f startLoc = new Vector3f(startX, startY, startZ);

        float look = readJnlpProperty("look");
        Quaternion startLook = new Quaternion(
                new float[] { 0f, (float) Math.toRadians(look), 0f });
        
        // connect to the default server
        try {
            loadServer(serverURL, startLoc, startLook);
        } catch (IOException ioe) {
            LOGGER.log(Level.WARNING, "Error connecting to default server "
                    + serverURL, ioe);
        }
    
    }    
    
    /**
     * Reads a system property taking into account that webstart only pass system properties which begins with jnpl.
     * @param property raw property name. Without jnlp.
     * @return the numeric representation of the property or zero if it wasn't found
     * @throws NumberFormatException if the property value doesn't represent a number
     */
    private float readJnlpProperty(String property) throws NumberFormatException {
        
        String jnlpProperty = System.getProperty("jnlp." + property);
        LOGGER.fine(String.format("property jnlp.%s = %s", property, jnlpProperty));
        float value;
        if (jnlpProperty == null) {
            value = Float.parseFloat(System.getProperty(property, "0"));
            LOGGER.fine(String.format("property %s = %f", property, value));
        } else {
            value = Float.parseFloat(jnlpProperty);
        }
        return value;
    }
    
    protected void loadServer(String serverURL, Vector3f translation,
            Quaternion look)
            throws IOException {
        LOGGER.info("[JmeClientMain] loadServer " + serverURL);

        logout();
        
        // get the login manager for the given server
        ServerSessionManager lm = LoginManager.getSessionManager(serverURL);

        // set the initial position, which will bne sent with the initial
        // connection properties of the cell cache connection
        login.setInitialPosition(translation, look);

        // OWL issue #185: set this manager as primary before creating any
        // connectons (but after it is properly initialized)
        login.setPrimary(true);
        
        // create a new session
        try {
            curSession = lm.createSession(login);
        } catch (LoginFailureException lfe) {
            IOException ioe = new IOException("Error connecting to "
                    + serverURL);
            ioe.initCause(lfe);
            throw ioe;
        }

        // make sure we logged in successfully
        if (curSession == null) {
            LOGGER.log(Level.WARNING, "Unable to connect to session");
            return;
        }

        frame.connected(true);

        // set the primary session
        lm.setPrimarySession(curSession);
        frame.setServerURL(serverURL);
  
    }
    
    /**
     * Create all of the Swing windows - and the 3D window
     */
    private void createUI(WorldManager wm, int width, int height) {
        JmeClientMain.frame = new org.jdesktop.wonderland.client.jme3.MainFrameImpl();
//        // center the frame
//        frame.getFrame().setLocationRelativeTo(null);
//
//        // show frame
//        frame.getFrame().setVisible(true);
//        
//        final JPanel canvas3D = frame.getCanvas3DPanel();
        // Initialize an onscreen view
        ViewManager.initialize(width, height);

        // This call will block until the render buffer is ready, for it to
        // become ready the canvas3D must be visible
        // Note: this disables focus traversal keys for the canvas it creates.
        ViewManager viewManager = ViewManager.getViewManager();
//        viewManager.attachViewCanvas(canvas3D);

        // Initialize the input manager.
        // Note: this also creates the view manager.
        // TODO: low bug: we would like to initialize the input manager BEFORE
        // frame.setVisible. But if we create the camera before frame.setVisible
        // the client window never appears.
//        CameraComponent cameraComp = viewManager.getCameraComponent();
        InputManager inputManager = ClientContext.getInputManager();
//        inputManager.initialize(frame.getCanvas(), cameraComp);

        // Default Policy: Enable global key and mouse focus everywhere
        // Note: the app base will impose its own (different) policy later
//        inputManager.addKeyMouseFocus(inputManager.getGlobalFocusEntity());

        /* For Testing FocusEvent3D
        InputManager3D.getInputManager().addGlobalEventListener(
        new EventClassListener () {
        private final Logger logger = Logger.getLogger("My Logger");
        public Class[] eventClassesToConsume () {
        return new Class[] { FocusEvent3D.class };
        }
        public void commitEvent (Event event) {
        logger.severe("Global listener: received mouse event, event = " + event);
        }
        });
         */

        /* Note: Example of global key and mouse event listener
        InputManager3D.getInputManager().addGlobalEventListener(
        new EventClassFocusListener () {
        private final Logger logger = Logger.getLogger("My Logger");
        public Class[] eventClassesToConsume () {
        return new Class[] { KeyEvent3D.class, MouseEvent3D.class };
        }
        public void commitEvent (Event event) {
        // NOTE: to test, change the two logger.fine calls below to logger.warning
        if (event instanceof KeyEvent3D) {
        if (((KeyEvent3D)event).isPressed()) {
        logger.fine("Global listener: received key event, event = " + event );
        }
        } else {
        logger.fine("Global listener: received mouse event, event = " + event);
        MouseEvent3D mouseEvent = (MouseEvent3D) event;
        System.err.println("Event pickDetails = " + mouseEvent.getPickDetails());
        System.err.println("Event entity = " + mouseEvent.getEntity());
        }
        }
        });
         */

        // make sure we don't miss any MT-Game exceptions
//        SceneWorker.addWorker(new WorkProcessor.WorkCommit() {
//            public void commit() {
//                Thread.currentThread().setUncaughtExceptionHandler(ueh);
//            }
//        });
//
//        frame.setDesiredFrameRate(getDesiredFrameRate());
//
//        // OWL issue #146: for full screen, make sure the canvas is actually
//        // the right size by forcing a repaint
//        canvas3D.invalidate();
//        frame.getFrame().getContentPane().validate();
        // start MTGame renderer
//        ClientContextJME.getWorldManager().getRenderManager().setRunning(true);
    }
    
    /**
     * logs out
     */
    protected void logout() {
        LOGGER.info("[JMEClientMain] log out");

        // disconnect from the current session
        if (curSession != null) {
            if (curSession.getStatus() == Status.CONNECTED) {
                synchronized (this) {
                    loggingOut = true;
                }
            }

            curSession.getCellCache().unloadAll();

            curSession.logout();
            curSession = null;
            frame.connected(false);

            // notify listeners that there is no longer a primary server
            LoginManager.setPrimary(null);
        }
    }
    
    
}
