/**
 * Project Wonderland
 *
 * Copyright (c) 2004-2009, Sun Microsystems, Inc., All Rights Reserved
 *
 * Redistributions in source code form must reproduce the above
 * copyright and this condition.
 *
 * The contents of this file are subject to the GNU General Public
 * License, Version 2 (the "License"); you may not use this file
 * except in compliance with the License. A copy of the License is
 * available at http://www.opensource.org/licenses/gpl-license.php.
 *
 * Sun designates this particular file as subject to the "Classpath" 
 * exception as provided by Sun in the License file that accompanied 
 * this code.
 */
package org.jdesktop.wonderland.client.jme.cellrenderer;

import com.jme3.bounding.BoundingSphere;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import org.jdesktop.wonderland.client.cell.Cell;
import org.jdesktop.wonderland.common.ExperimentalAPI;

/**
 * Renderer for Avatar, looks strangely like a teapot at the moment...
 * 
 * @author paulby
 */
@ExperimentalAPI
public class AvatarJME extends BasicRenderer {

    public AvatarJME(Cell cell) {
        super(cell);
    }

    @Override
    protected Node createSceneGraph(Spatial entity) {
        ColorRGBA color = new ColorRGBA();
        
        color.r = 0.0f; color.g = 0.0f; color.b = 1.0f; color.a = 1.0f;
        Node ret = createTeapotEntity(cell.getCellID().toString(), color);        

        return ret;
    }

    public Node createTeapotEntity(String name, 
            ColorRGBA color) {
//        MaterialState matState = null;
        
        // The center teapot
        Node ret = new Node();
        Geometry teapot = new Geometry("Teapot", new Sphere());
        teapot.setLocalScale(0.2f);
        ret.attachChild(teapot);

//        new Material( );
//        matState = (MaterialState) ClientContextJME.getWorldManager().getRenderManager().createRendererState(RenderState.RS_MATERIAL);
//        matState.setDiffuse(color);
//        ret.setRenderState(matState);

        ret.setModelBound(new BoundingSphere());
        ret.updateModelBound();

        return ret;
    }

}
