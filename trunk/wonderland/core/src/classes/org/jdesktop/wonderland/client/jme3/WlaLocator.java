/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jdesktop.wonderland.client.jme3;

import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetKey;
import com.jme3.asset.AssetLocator;
import com.jme3.asset.AssetManager;
import com.jme3.asset.plugins.UrlAssetInfo;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WlaLocator implements AssetLocator {

    private static final Logger logger = Logger.getLogger(WlaLocator.class.getName());

    @Override
    public void setRootPath(String rootPath) {
    }

    @Override
    public AssetInfo locate(AssetManager manager, AssetKey key) {
        String name = key.getName();
        try{
            URL url = new URL(name);
            return UrlAssetInfo.create(manager, key, url);
        }catch (FileNotFoundException e){
            return null;
        }catch (IOException ex){
            logger.log(Level.WARNING, "Error while locating " + name, ex);
            return null;
        }
    }


}
